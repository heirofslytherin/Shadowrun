import os
import pygame
import sys
import glob
from collections import namedtuple

AnimRect = namedtuple("AnimRect", "image rect")

pygame.init()

graphics_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),"..","resources", "graphics"))
joshua_path = os.path.join(graphics_dir, "josh.bmp")
josh = pygame.image.load(joshua_path)
josh_rect = josh.get_rect()

overlays_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),"..","resources", "map"))
redmond_barrens_path = os.path.join(overlays_dir, "RedmondBarrens.png")
redmond_barrens = pygame.image.load(redmond_barrens_path)
redmond_barrens_rect = redmond_barrens.get_rect()

joshua_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),"..","resources", "graphics", "JoshuaSamurai", "Walking"))
files = os.listdir(joshua_dir)
file_paths = [os.path.join(joshua_dir, file) for file in files]

size = tuple([5*x for x in josh.get_size()])
screen = pygame.display.set_mode(redmond_barrens.get_size())

class Character(pygame.sprite.Sprite):

    def __init__(self, image, position = (0,0), img_dir = None):
        pygame.sprite.Sprite.__init__(self)

        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.move_ip(position[0], position[1])
        self.store_animations(directory=img_dir, position=position)
        self.animation_idx_x = 0

    def store_animations(self, directory, position):
        files = os.listdir(joshua_dir)
        file_paths = [os.path.join(joshua_dir, file) for file in files]
        self.right_img_dict = {key:AnimRect(pygame.image.load(val),pygame.image.load(val).get_rect()) for key, val in zip(files, file_paths) if "right" in val}
        self.left_img_dict = {key: AnimRect(pygame.image.load(val), pygame.image.load(val).get_rect()) for key, val in
                          zip(files, file_paths) if "left" in val}
        for key in self.right_img_dict:
            self.right_img_dict[key].rect.move_ip(position[0], position[1])
        for key in self.left_img_dict:
            self.left_img_dict[key].rect.move_ip(position[0], position[1])

    def move(self, amount=(0,0)):
        dx = amount[0]
        dy = amount[1]
        self.rect.move_ip(dx, dy)

        if amount[0] > 0:
            inc = 1
            dictionary = self.right_img_dict
        elif amount[0] < 0:
            inc = -1
            dictionary = self.left_img_dict
        self.animation_idx_x = (self.animation_idx_x + inc) % len(dictionary)
        self.image = dictionary[[x for x in dictionary.keys()][self.animation_idx_x]].image
        self.rect = dictionary[[x for x in dictionary.keys()][self.animation_idx_x]].rect

class Background(pygame.sprite.Sprite):

    def __init__(self, image):
        pygame.sprite.Sprite.__init__(self)

        self.image = image.convert()
        self.rect = self.image.get_rect()

class NPCs(pygame.sprite.RenderPlain):

    def __init__(self):
        pygame.sprite.Group.__init__(self)
        pass

class Backgrounds(pygame.sprite.RenderPlain):

    def __init__(self):
        pygame.sprite.Group.__init__(self)
        pass


josh_char = Character(image=josh, position=(40,40))
npcs = NPCs()
npcs.add(josh_char)

map = Background(image = redmond_barrens)
layers = Backgrounds()
layers.add(map)

pygame.key.set_repeat(40, 40)


while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT: josh_char.move((5, 0))
            if event.key == pygame.K_LEFT: josh_char.move((-5, 0))
            if event.key == pygame.K_UP: josh_char.move((0, -5))
            if event.key == pygame.K_DOWN: josh_char.move((0, 5))
        # keys = pygame.key.get_pressed()
        # if keys[pygame.K_UP]: josh_char.move((0, -5))
        # if keys[pygame.K_DOWN]: josh_char.move((0, 5))
        # if keys[pygame.K_LEFT]: josh_char.move((-5, 0))
        # if keys[pygame.K_RIGHT]: josh_char.move((5, 0))

    layers.draw(screen)
    npcs.draw(screen)

    pygame.display.flip()